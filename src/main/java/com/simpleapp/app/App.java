package com.mycompany.app;

public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello!" );
        System.out.println( "Today we learn about GitLab Best Practices and CI/CD magic :)" );
        System.out.println( "" );
        System.out.println("\u001B[33m           +                        +");
        System.out.println("          :s:                      :s:");
        System.out.println("         .oso'                    'oso.");
        System.out.println("         +sss+                    +sss+");
        System.out.println("        :sssss-                  -sssss:");
        System.out.println("       'ossssso.                .ossssso'");
        System.out.println("       +sssssss+                +sssssss+");
        System.out.println("      -ooooooooo-++++++++++++++-ooooooooo-\u001B[0m");
        System.out.println("\u001B[37m    ':/" + "\u001B[31m+++++++++" + "\u001B[33mosssssssssssso" + "\u001B[31m+++++++++" + "\u001B[37m/:'\u001B[0m\u001B[0m");
        System.out.println("\u001B[37m    -///" + "\u001B[31m+++++++++" + "\u001B[33mcssssssssssss" + "\u001B[31m+++++++++" + "\u001B[37m///-\u001B[0m");
        System.out.println("\u001B[37m   .//////" + "\u001B[31m+++++++" + "\u001B[33mcosssssssssso" + "\u001B[31m+++++++" + "\u001B[37m//////.\u001B[0m");
        System.out.println("\u001B[37m   :///////" + "\u001B[31m+++++++" + "\u001B[33mosssssssso" + "\u001B[31m+++++++" + "\u001B[37m///////:\u001B[0m");
        System.out.println("\u001B[37m    .:///////" + "\u001B[31m++++++" + "\u001B[33mssssssss" + "\u001B[31m++++++" + "\u001B[37m///////:.'\u001B[0m");
        System.out.println("\u001B[37m      '-://///" + "\u001B[31m+++++" + "\u001B[33mosssssso" + "\u001B[31m+++++" + "\u001B[37m/////:-'\u001B[0m");
        System.out.println("\u001B[37m         '-:////" + "\u001B[31m++++" + "\u001B[33mosssso" + "\u001B[31m++++" + "\u001B[37m////:-'\u001B[0m");
        System.out.println("\u001B[37m            .-:///" + "\u001B[31m++" + "\u001B[33mosssso" + "\u001B[31m++" + "\u001B[37m///:-.\u001B[0m");
        System.out.println("\u001B[37m              '.://" + "\u001B[31m++" + "\u001B[33mcosso" + "\u001B[31m++" + "\u001B[37m//:.'\u001B[0m");
        System.out.println("\u001B[37m                 '-:/" + "\u001B[31m+" + "\u001B[33mcoo" + "\u001B[31m+" + "\u001B[37m/:-'\u001B[0m");
        System.out.println("\u001B[37m                    '-" + "\u001B[31m++" + "\u001B[31m-'\u001B[0m");
        System.out.println("\n\n" + "\u001B[36m          Tanuki has been summoned...\u001B[0m" + "\n\n");
        
    }
}
